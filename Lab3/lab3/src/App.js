
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Creat from './components/Creat';
function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <header className="App-header">
    
        <p>
        Let's listen to the song
        </p>
        <a href="https://joox.com">มาฟังเพลงกันเถอะ </a>
      </header>
      <Creat />
      <Footer/>
    </div>
  );
}

export default App;
