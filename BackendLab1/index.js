const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result ={}

    if ( !isNaN(weight) && !isNaN(height) ){
      let bmi = weight / (height * height)
      result = {
        "status" : 200,
        "bmi" : bmi
      }
    }else {

        result = {
          "status" : 400,
          "message" : "Weight or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))
})


app.get('/triangle', (req, res) => {
  let base = parseFloat(req.query.base)
  let height = parseFloat(req.query.height)

  let triangle =  0.5 * base * height
  res.send("TRIANGLE = " +triangle)
  
})



app.post('/score', (req, res) => {
  
  let score = parseFloat(req.query.score)
  var grade ={}

    if( score >= 90 ) {
    grade = "A"
  } else if( score >= 85 ) {
    grade = "B+"
  } else if( score >= 80 ) {
    grade = "B"
  } else if( score >= 75 ) {
    grade = "C+"
  } else if( score >= 70 ) {
    grade = "C"
  } else if( score >= 65 ) {
    grade = "D+"
  } else if( score >= 60 ) {
    grade = "D"
  } else if( score < 50 ){
    grade = "E"
  }

res.send(JSON.stringify(req.query.name + "เกรดที่ได้ : " + grade))
})


app.get('/hello', (req, res) => {
    res.send('Sawasdee '+req.query.name)
  })
  

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})