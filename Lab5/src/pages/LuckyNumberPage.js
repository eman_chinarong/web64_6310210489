import LuckyNumber from "../components/LuckyNumber";
import { useState } from "react";
import { Typography,Box, } from "@mui/material";
import Button from '@mui/material/Button';

function LuckyNumberPage() {

    const [ number, setNumber ] = useState("");
    const [ result, setResult ] = useState(0);
    
    function Number() {
        let n = parseInt(number);
        let result = n;
        setResult(result);
        if (number == 69){
            setResult("ถูกแล้วจ้า")
        }else {
            setResult("ผิด")
        }
    }

    return(
        
        <Box sx={{ textAlign : 'center'}}>
                    <Typography variant="h5" sx = {{marginTop : "10px"}}>
                    กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99:         
                    </Typography>
                    <br />
            <input type="text"
                            value={number}
                            onChange={ (e) => { setNumber(e.target.value); } } /> 
                            <br /> 
                            <br />
      
            { result != 0 &&
                <div>
                    <hr />
                    <LuckyNumber 
                    result = {result} 
                    />
                </div>

            }
            <Button variant="contained"  onClick={ ()=>{ Number() } }> 
            ทาย 
        </Button>
        </Box>
       
    
    );
}

export default LuckyNumberPage;