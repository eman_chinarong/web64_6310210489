function BMIResult (props) {

    return (
        <div>
          <h2>คุณ : {props.name} </h2>
          <h3>มี BMI : {props.bmi} </h3>
          <h3>แปลว่า : {props.result} </h3>
        </div>
    );
}
export default BMIResult;